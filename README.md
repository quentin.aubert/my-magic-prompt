# My Magic Prompt

### Project
This is a shell script to create a prompt to learn shell-scripting

#### Commmands

| Command | Argument(s) | Description                    |
|:------------:|:----|-----------------------------:|
| `about` | / | What is this program? |
| `age` | / | What is your age, I tell you if your are an adult or not! |
| `cd` | folder_name | Change the working directory |
| `help`| / | Display the help window   |
| `hour` | / |  Get current time  |
| `httpget` | website_url | Download index file from a website and change filename |
| `ls`   | / |   List directory contents  |
| `open` | file_name | Edit file with vim editor |
| `passw` | / | Change script access password with confirmation |
| `profile` | / | Who is the author of this script |
| `pwd` | / | Print the full filename of the current working directory |
| `quit` | / | Quit the prompt |
| `rm` | file_name | Remove file |
| `rmd` or `rmdir` | folder_name | Remove folder |
| `rmdirwtf` | file(s)_name(s) and / or  folder(s)_name(s) | Remove folder with password verification |
| `rps` | / | Play with your friend Rock Paper Scissors (not done) |
| `smtp` | / | Send an email with subject and body to my email address |
| `version` or `vers` or `--v` | / | Print the version of this prompt |
| `*` | / | This command is unknown |


#### Features

- packages:
postfix : use command `apt get-install postfix` on ubuntu distribution
mailutils : https://mailutils.org/ or use command `apt get-install mailutils`
- If you are french use this link to configure your postfix with gmail : [Postfix Configuration](https://www.it-connect.fr/configurer-postfix-pour-envoyer-des-mails-avec-gmail/ "Postfix Configuration")

## End