#!/usr/bin/bash
# Bash prompt for execute commands
mainPassword="test"

about() {
  # use to tell what is this program
  echo "I am My Magic Prompt"
}

age() {
  # Ask your age and tell you if your an adult or a child
  read -p "How old are you ? : " age
  if [ "$age" -ge 18 ]; then
    echo "You are an adult."
  elif [ "$age" -eq 0 ]; then
    echo "You are just born."
  elif [ "$age" -lt 0 ]; then
    echo "You are not born (impossible)."
  else echo "You are a child."
  fi
}

fct_rm() {
  # use to remove a file
  if [ -f "${tab[1]}" ]; then
    rm ${tab[1]}
    echo "The file named ${tab[1]} was delete."
  else echo "This file doesn't exist."
  fi
}

fct_rmd() {
  # use to remove a folder
  if [ -d "${tab[1]}" ]; then
    rm -r ${tab[1]}
    echo "The folder named ${tab[1]} was delete."
  else echo "This folder not exist. (don't forget to put your folder name after the command)"
  fi
}

helpList() {
  # clear screen then display help list
  clear
  echo -e "\033[7mabout\033[0m : What is this program?"
  echo -e "\033[7mage\033[0m : What is your age, I tell you if your are an adult or not!"
  echo -e "\033[7mcd\033[0m : Change the working directory"
  echo -e "\033[7mhelp\033[0m : Display the help"
  echo -e "\033[7mhour\033[0m : Get current time."
  echo -e "\033[7mhttpget\033[0m : Download index file from a website and change filename."
  echo -e "\033[7mls\033[0m : List directory contents"
  echo -e "\033[7mopen\033[0m : Edit file with vim editor."
  echo -e "\033[7mpassw\033[0m : Change script access password with confirmation"
  echo -e "\033[7mprofile\033[0m : Who is the author of this script"
  echo -e "\033[7mpwd\033[0m : Print the full filename of the current working directory."
  echo -e "\033[7mquit\033[0m : Quit the prompt"
  echo -e "\033[7mrm\033[0m : Remove file"
  echo -e "\033[7mrmd\033[0m or \033[7mrmdir\033[0m : Remove folder"
  echo -e "\033[7mrmdirwtf\033[0m : Remove folder with password verification"
  echo -e "\033[7mrps\033[0m : Play with your friend \033[1mRock Paper Scissors\033[0m."
  echo -e "\033[7msmtp\033[0m : Send an email with subject and body to my email address."
  echo -e "\033[7mversion\033[0m or \033[7m--v\033[0m or \033[7mvers\033[0m :  Print the version of this prompt"
  echo -e "\033[7m*\033[0m : This command is unknown"
  echo
}

httpget() {
  # Download source code of a Website
  read -p "Name of the file where you want to save the source code : " file_sourcecode
  wget -qO $file_sourcecode ${tab[1]}
  echo "The source code was correctly save in $file_sourcecode."
}

passwd() {
  # Change Password with confirmation
  read -s -p "Please enter your old password : " testpw
  if [ "$testpw" != "$mainPassword" ]; then
    echo
    echo "Wrong Password, retry !"
    passwd
  fi
  echo
  read -s -p "New Password : " passwd_change
  echo
  read -p "Are you sure ? y/n : " confirmation
  if [ "$confirmation"=="y" ]; then
    mainPassword=$passwd_change
    echo "Password changed"
  else echo "Password didn't change"
  fi
}

printHour(){
  # Print the hour at the instant
  date +"%H:%M"
}

profile() {
  # write information about the author
  echo "Nom : Aubert"
  echo "Prénom : Quentin"
  echo "age : 22 years old"
  echo "mail : quentin.aubert42@gmail.com"
}

removewtf() {
  # Remove folder(s) and file(s) with a password check
  read -s -p "Your password to validate this action please : " pass
  if [ "$pass" != "$mainPassword" ]; then
    echo "Your password is wrong"
    removewtf
  else rm -r ${tab[@]}
    echo "Folder(s) and file(s) correctly deleted"
  fi
}

rps(){
  # Use to play Rock Paper Scissors (not release)
  echo "This game isn't release now !"
}

sendMail() {
  # Send an email with subject and body
  read -p "Mail To : " mailAddress
  read -p "Subject : " subject
  read -p "Body : " body
  echo "$body" | mail -s "$subject" $mailAddress
  echo "email send"
}

version() {
  # Print version of this program
  echo "My Magic Prompt version 1.0.0"
}

isSecure(){
  # Password Check
  read -p "Enter your login : " login
  read -s -p "Enter your password : " password
  echo
  if [ "$login" != "quentin" ] || [ "$password" != "$mainPassword" ]; then
    echo "Unvalid login or password."
    isSecure
  fi
}

prompt(){
  while true 
  do
  read -p "Type a command : " arg0
    tab=($arg0)
    cmd="${tab[0]}"
    unset tab[0]
    case $cmd in
      about ) about;;
      age ) age;;
      cd ) cd ${tab[1]};;
      help ) helpList;;
      hour ) printHour;;
      httpget ) httpget ${tab[1]};;
      ls ) ls -a;;
      open ) vim ${tab[1]};;
      passw ) passwd;;
      profile ) profile;;
      pwd ) pwd;;
      quit ) exit;;
      rps ) rps;;
      rm ) fct_rm;;
      rmd | rmdir ) fct_rmd;;
      rmdirwtf ) removewtf;;
      smtp ) sendMail;;
      version | --v | vers ) version;;
      * ) echo "Command Invalid";;
    esac
  done
}

main() {
  isSecure
  prompt
}

main